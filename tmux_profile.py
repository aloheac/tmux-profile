#!/usr/bin/env python3

from pathlib import Path
from pprint import pprint
from string import Template

import os
import os.path
import json

# Return the existing configuration directory with the highest priority
def get_cfg_dir():
    home = str(Path.home())
    env_var = os.getenv("TMUX_PROFILE") if os.getenv("TMUX_PROFILE") else ""

    possible_cfg_dir = [
        env_var,
        "tmux_profiles",
        home + "/.config/tmux_profile",
        "/etc/tmux_profile",
    ]

    for dir in possible_cfg_dir:
        if os.path.isdir(dir):
            return dir

    raise RuntimeError("No configuration directory found")

# Function to get all *.json files in a directory
def get_json_files(dir):
    all_files = os.listdir(dir)
    cfg_files = list(
        filter(lambda f: f.endswith(".tpf") or f.endswith(".tpl.tpf"),
               all_files))
    cfg_files.sort()
    return cfg_files

def list_choices(choices):
    for idx in range(0, len(choices)):
        print(str(idx) + ": " + choices[idx])

def parse_choices(choices_str, min_val, max_val):
    choices = []
    for choice in choices_str.split():
        choice = choice.strip()
        if choice == "":
            continue
        val = int(choice)
        if val < min_val or val > max_val:
            raise ValueError()
        choices.append(val)
    return choices

def get_array_entries(indexes, array):
    entries = []
    for idx in indexes:
        entries.append(array[idx])
    return entries

# Prompt user to choose element(s) from a list
def cfg_chooser(files, allow_multiple_choice = False):
    choices = None

    prompt = ("What configuration(s) file(s) to use? "
              if allow_multiple_choice
              else "What configuration file to use? ")

    while not choices:
        list_choices(files);
        str = input(prompt)
        try:
            choices_idx = parse_choices(str, 0, len(files))
            choices = get_array_entries(choices_idx, files)
            if not allow_multiple_choice and len(choices) > 1:
                print("Only one choice allowed")
                choices = None
        except ValueError:
            pass

    pprint(choices)
    return choices

def read_file(filename):
    f = open(filename)
    content = f.read()
    f.close()
    return content

def convert_to_json(cfg_str, tpl_dict):
    if tpl_dict:
        template = Template(cfg_str)
        cfg_str = template.substitute(tpl_dict)
    cfg_json = json.loads(cfg_str)
    return cfg_json

# Read the workspaces file and return paths associated with their names
def read_workspaces(filename):
    content = read_file(filename)
    all_ws = json.loads(content)
    workspaces = {}
    for ws in all_ws:
        workspaces[ws["name"]] = ws
    return workspaces

# Function to create tmux session and window
def create_tmux_window(session_name, window_name):
    print("New window %s in session %s" % (window_name, session_name))

    if os.system('tmux has-session -t "%s"' % session_name) == 0:
        print("Session already exists, only create window")
        print('tmux new-window -t "%s" -n "%s"'
              % (session_name, window_name))
        os.system('tmux new-window -t "%s" -n "%s"'
                   % (session_name, window_name))
    else:
        print("Creating session and window")
        os.system('tmux new-session -s "%s" -d' % session_name)
        os.system('tmux rename-window -t "%s" "%s"'
                   % (session_name, window_name))

# Function to split window
def split_tmux_window(session_name, window_name, is_vertical_split = True):
    target_window = session_name + ':' + window_name
    print(("Split window '%s' %s"
           % (target_window,
              "vertically" if is_vertical_split else "horizontally"))
    )
    orientation = "-v" if is_vertical_split else "-h"
    os.system('tmux split-window -t "%s" %s' % (target_window, orientation))

# Function to run command in window
def run_command_in_tmux_window(session_name, window_name, command):
    target_window = session_name + ':' + window_name
    print("Run in window '%s' command '%s'" % (target_window, command))
    os.system('tmux send-keys -t "%s" "%s" C-m' % (target_window, command))

# Function to run profile
def run_profile(cfg):
    session_name = cfg["session-name"]
    window_name = cfg["window-name"]

    create_tmux_window(session_name, window_name)

    for panel in cfg["panels"]:
        if "vertical-split" in panel:
            is_vertical = True if panel["vertical-split"] == "True" else False
            split_tmux_window(session_name, window_name, is_vertical)

        if "path" in panel:
            cmd = "cd %s" % panel["path"]
            run_command_in_tmux_window(session_name, window_name, cmd)

        if "command" in panel:
            cmd = panel["command"]
            run_command_in_tmux_window(session_name, window_name, cmd)
        elif "example-command" in panel:
            cmd = "echo '%s'" % panel["example-command"]
            run_command_in_tmux_window(session_name, window_name, cmd)

    print("Done")
    print("Attach to session with:")
    print("tmux attach-session -t '%s:%s'" % (session_name, window_name))

# Return True if one element of the list ends with some string
def list_contains_ext(elts, ext):
    l = list(filter(lambda f: f.endswith(ext), elts))
    return True if len(l) else False

# Function main (ask which profile then run profile)
def main():
    template_dict = None
    cfg_dir = get_cfg_dir()
    cfg_all_files = get_json_files(cfg_dir)
    cfg_chosen_files = cfg_chooser(cfg_all_files, True)

    if list_contains_ext(cfg_chosen_files, ".tpl.tpf"):
        ws_list = read_workspaces(cfg_dir + "/workspaces.cfg")
        ws_name = cfg_chooser(list(ws_list.keys()), False)[0]
        template_dict = ws_list[ws_name]

    for cfg_file in cfg_chosen_files:
        cfg_str = read_file(cfg_dir + "/" + cfg_file)
        cfg = convert_to_json(cfg_str, template_dict)
        run_profile(cfg)

if __name__=="__main__":
    main()
