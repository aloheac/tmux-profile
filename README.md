# tmux profile

Offers to open a layout in a tmux session

## Example

Simple configuration files are in the `tests/tmux_profiles` directory.

The first example show how to start a tmux session (if it is not started yet),
create a new window named "editor_and_run" and place in it one panel for
editing the program, a second panel to run the program but not started yet and
the third panel to run a simulator (this one is started)

```bash
cd tests
../tmux_profile.py
# Use profile '0'
```

When the profile is run, the command to attach to the tmux session will be
printed. This should be something like that:

```bash
tmux attach-session -t 'tmux_profile_example:editor_and_run'
```

## Configuration

### Example

Here is a commented example:

```json
// An example of a new window with a single panel
{
    "session-name": "runner",   // The [future] tmux session name
    "window-name": "project01", // The future window name
    "panels": [                 // The list of panels
        {
            "path": "tests/first-project", // Should be absolute
            "command": "nano main.py"      // Command run in the terminal
        }, {
            "vertical-split": "False",     // 2nd window on the left
            "path": "tests/first-project", // Should be absolute
            "command": "./hello_world.py"  // Command run in the terminal
        }, {
            "vertical-split": "True",      // 3rd window below
            "path": "tests/first-project", // Should be absolute
            "example-command": "./simu.py" // This won't be executed
        }
    ]
}
```

Some parameters explained:

* `"command": "<command>"` will run the command
* `"example-command": "<command>"` will print the command, but not run it
* `"vertical-split": "False"` will split vertically
* `"vertical-split": "True"` will split horizontally

### Location

The configuration files must be in one of this configuration directory:

* `$TMUX_PROFILE`
* `./tmux_profiles` (note the extra 's')
* `~/.config/tmux_profile`
* `/etc/tmux_profile`

If more than one exists, the first will be used in the order they are listed.

A configuration file should have the `.tpf` (Tmux ProFile) extension.

## Templates

Sometimes there are very little differences between two configuration
files. One example is when the same project is at two different locations. In
this case we want to have the same profiles with the two possible locations.

Create a file in the configuration directory named `workspaces.cfg` and list
the different "projects".

A project is a JSON object with key-values:

```json
[
    {
        "path": "first-project",
        "name": "FP"
    },
    {
        "path": "second-project",
        "name": "SP"
    }
]
```

A configuration file using template should have the `.tpl.tpf` extension.

The `name` key is mandatory and will be used (among other internal things) to
list all projects and prompt user to choose one.

In a template configuration file, use `${key}` whenever needed, it will be
replaced at runtime.

Here is an example:

```json
{
    "session-name": "tmux_profile_example",
    "window-name": "${name}",
    "panels": [
        {
            "path": "${path}",
            "command": "nano hello_world.py"
        }, {
            "vertical-split": "False",
            "path": "${path}",
            "example-command": "./hello_world.py"
        }, {
            "vertical-split": "True",
            "path": "${path}",
            "command": "./simu.py"
        }
    ]
}
```

## Usage

Make sure a configuration directory and at least one configuration file exist.

Start the program:

```
./tmux_profile.py
```

The program will list all profiles in the configuration directory with an
index before before their name.

Select one or multiple configuration files by entering their index separated
by spaces then hit enter.

If one of the configuration file is a template, the program will now list the
workspaces with an index before their name

Select one workspace by entering it's index (only one can be selected) then
hit enter.

The program will run the profile (this will display logs). Then it will print
the command to attach to the tmux session. If you're not yet attached to the
session you can use this command to do so.
